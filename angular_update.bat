rem * * * Author marcos.cruz@omni.com.br
echo off 
cls
echo Atualizando %1%
echo changing directory para %1%
cd %1%

echo Deletando .npmrc
del .npmrc

echo limpando cache
call npm cache clean --force

SET OMNI_REGISTRY="https://nexus.omni.com.br/repository/npm-group/"
SET NPM_REGISTRY="https://registry.npmjs.org/"

echo Configurando OMNI Nexus como registry
call npm config set registry %OMNI_REGISTRY%
SET OMNI_TABLE="@omni/table@0.2.8"

echo Instalando %OMNI_TABLE%
call npm install --save %OMNI_TABLE%
call npm install

echo Voltando registry para %NPM_REGISTRY%
call npm config set registry %NPM_REGISTRY%

echo instalando typescript 
call npm install --save typescript

echo executando npx browserslist@latest --update-db
call npx browserslist@latest --update-db
echo Instalando dependencias 'npm install'
call npm install
echo Executando npm install --save-dev @angular-devkit/build-ng-packagr@~0.901.7 ng-packagr tsickle
npm install --save-dev @angular-devkit/build-ng-packagr@~0.901.7 ng-packagr tsickle
call omni pack
call omni build


echo Não esquecer de criar single spa: 
echo * ng add single-spa-angular
echo * configurar o selector em src/main.single-spa.ts
cd ..